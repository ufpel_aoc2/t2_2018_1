
#include "simplegrade.h"
#include "sim.h"




#define FREE(a) if (a) free(a);

void test1(){
	struct stats * res;

	WHEN("Testes de parâmetros");
	IF("Parâmetros inválidos");
	THEN("Deve retornar NULL");

	res = sim(8, 16384, 4, NULL, "R 0000000A\nW 00000014\n");
	isNull(res,1);
	FREE(res);

	res = sim(32, 4096, 4, NULL, "R 0000000A\nW 00000014\n");
	isNull(res,1);
	FREE(res);

	res = sim(32, 16384, 1, NULL, "R 0000000A\nW 00000014\n");
	isNull(res,1);
	FREE(res);

	res = sim(32, 16384, 4, NULL, NULL );
	isNull(res,1);
	FREE(res);
}

void test2(){
	struct stats *res;

	WHEN("Testes simples com traços curtos");
	IF("Duas leituras no mesmo bloco");
	THEN("Deve dar hit na segunda na tlb e na tlb");
	res = sim(32, 16384, 4, NULL, "R 0000000A\nR 0000000B\n");
	// tlb = 51 + 1
	// cache = 52 + 2
	// total = 52 + 54 = 106
	
	if(res){
		isEqual(res->cycles,106,1);
		FREE(res);
	}
	else isNotNull(res,1);

	IF("Duas leituras no mesmo bloco, outra cache");
	THEN("Deve dar hit na segunda op na tlb e na tlb");
	res = sim(32, 8192, 4, NULL, "R 0000000A\nR 0000000B\n");
	// tlb = 51 + 1
	// cache = 51 + 1
	// total = 52 + 52 = 104
	
	if(res){
		isEqual(res->cycles,104,1);
		FREE(res);
	}
	else isNotNull(res,1);	

	IF("Conflito de bloco");
	THEN("Deve dar miss na MD");
	// bloco de 32 B 
	// 16384 / 32 = 512 blocos
	// indice = 9 
	// offset = 5
	// então 2^15 vai dar o mesmo bloco = 32768 

	res = sim(32, 16384, 4, NULL, "R 0000000A\nR 00008000\nR 0000000B\n");
	// tlb = 51 + 51 + 1
	// cache = 52 + 52 + 52
	// total = 103 + 156 = 259
	
	if(res){
		isEqual(res->cycles,259,1);
		FREE(res);
	}
	else isNotNull(res,1);

	THEN("Não deve dar o terceiro miss na 2-associativa");
	res = sim(32, 8192, 4, NULL, "R 0000000A\nR 00008000\nR 0000000B\n");
	// tlb = 51 + 51 + 1
	// cache = 51 + 51 + 1
	// total = 103 + 103 = 206

	if(res){
		isEqual(res->cycles,206,1);
		FREE(res);
	}
	else isNotNull(res,1);


	IF("Estressa a TLB pequena");
	THEN("Deve dar seis misses");
	res = sim(32, 8192, 4, NULL, "R 0000000A\nR 00008000\nR 00001388\nR 0000270F\nR 00000D05\nR 0000000B\n");
	// tlb = 51 + 51 + 51 + 51 + 51 + 51  = 306
	// cache = 51 + 51 + 51 + 51 + 51 + 1  = 256
	// total = 562

	if(res){
		isEqual(res->misses_tlb,6,1);
		isEqual(res->misses_l1,5,1);
		isEqual(res->cycles,562,1);
		FREE(res);
	}
	else isNotNull(res,3);

	IF("Estressa a TLB maior");
	THEN("Deve dar cinco misses");
	res = sim(32, 8192, 8, NULL, "R 0000000A\nR 00008000\nR 00001388\nR 0000270F\nR 00000D05\nR 0000000B\n");
	// tlb = 52 + 52 + 52 + 52 + 52 + 2 = 262
	// cache = 51 + 51 + 51 + 51 + 51 + 1 = 256 
	// total = 518

	if(res){
		isEqual(res->misses_tlb,5,1);
		isEqual(res->misses_l1,5,1);
		isEqual(res->cycles,518,1);
		FREE(res);
	}
	else isNotNull(res,3);

}

void test3(){
	struct stats * res;

	WHEN("Teste simples de leitura de arquivo");
	IF("Arquivo está presente");
	THEN("Deve simular");

	res = sim(32, 16384, 4, "t1.txt", NULL);
	if (res){
		isEqual(res->cycles,156,2);
		FREE(res);
	}
	else isNotNull(res,2);
}


void test4(){
	struct stats * res;

	WHEN("Teste com MAC");
	IF("MAC com DM");
	THEN("Deve simular");

	res = sim(32, 16384, 4, "t2.txt", NULL);
	if (res){
		isEqual(res->cycles,10436,10);
		FREE(res);
	}
	else isNotNull(res,10);

	IF("MAC com 2-assoc");
	THEN("Deve simular");
	res = sim(32, 8192, 4, "t2.txt", NULL);
	if (res){
		isEqual(res->cycles,9924,10);
		FREE(res);
	}
	else isNotNull(res,10);

	IF("MAC com 2-assoc");
	THEN("Deve simular");
	res = sim(64, 8192, 4, "t2.txt", NULL);
	if (res){
		isEqual(res->cycles,8724,10);
		FREE(res);
	}
	else isNotNull(res,10);


}


int main(){
	test1();
	test2();
	test3();
	test4();

	GRADEME();

	if (grade==maxgrade)
		return 0;
	else return grade;



}
